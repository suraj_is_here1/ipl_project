const fs = require('fs')
const csvToJson = require('../data/csvtoJSONConverter');

try {
    function strikeRatePerSeason() {
        const matchesFilePath = 'src/data/matches.csv';
        const deliveriesFilePath = 'src/data/deliveries.csv';
        const outputFilePath = 'src/public/output.strikeRatePerSeason.json';



        let batsmanStrikeRatePerSeason = {};

        csvToJson(matchesFilePath).then((matches) => {
            let idWithSeason = {}

            for (const match of matches) {
                let season = match.season;
                let matchId = match.id
                idWithSeason[matchId] = season
            }

            csvToJson(deliveriesFilePath).then((deliveries) => {

                let allBatsMan = [];
                let seasonWitStats = {};
                let FinalData = {};
                for (const delivery of deliveries) {
                    let currentBatsman = delivery.batsman;
                    if (!(allBatsMan.includes(currentBatsman))) {
                        allBatsMan.push(currentBatsman);
                    }
                }
                for (const batsman of allBatsMan) {
                    let ballPerSeason = {};
                    let runPerSeason = {};

                    for (const delivery of deliveries) {
                        let currentBatsman = delivery.batsman;
                        if (batsman == currentBatsman) {
                            let currentSeason = idWithSeason[delivery.match_id];

                            let runs = Number(delivery.batsman_runs);
                            let balls = Number(delivery.ball);
                            if (delivery.wide_runs != '1') {
                                if (ballPerSeason[currentSeason]) {
                                    ballPerSeason[currentSeason] += 1
                                }
                                else {
                                    ballPerSeason[currentSeason] = 1
                                }
                                if (runPerSeason[currentSeason]) {
                                    runPerSeason[currentSeason] += runs;

                                }
                                else {
                                    runPerSeason[currentSeason] = runs;
                                }
                            }

                        }

                    }
                    let strikeRatePerSeason = {};
                    for (const season in runPerSeason) {
                        strikeRatePerSeason[season] = ((runPerSeason[season] / ballPerSeason[season]) * 100).toFixed(2)
                    }
                    batsmanStrikeRatePerSeason[batsman] = strikeRatePerSeason;
                }

                let StringifiedObject = JSON.stringify(batsmanStrikeRatePerSeason, null, 2);

                fs.writeFile(outputFilePath, StringifiedObject, (err) => {
                    if (err) {
                        console.log(error);
                    }
                    else {
                        console.log("File Created Successfully");

                    }
                })

            });
        });



    }
}
catch (error) {
    console.log(error);
}
strikeRatePerSeason()
