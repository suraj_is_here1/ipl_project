const fs = require('fs')
const csvToJson = require('../data/csvtoJSONConverter');

try {
    function highestPOMeachSeason() {
        const matchesFilePath = 'src/data/matches.csv';
        const deliveriesFilePath = 'src/data/deliveries.csv';
        const outputFilePath = 'src/public/output.highestPOMPerSeason.json';

        let PlayerWithHighestPOMPerSeason = {}
        csvToJson(matchesFilePath).then((match) => {

            let POMCount = {};

            for (let index = 0; index < match.length; index++) {

                let season = match[index].season;
                let PlayerOfTheMatch = match[index].player_of_match;
                let POMperPlayer = {}

                if (POMCount[season]) {

                    if (POMCount[season][PlayerOfTheMatch]) {

                        POMCount[season][PlayerOfTheMatch] += 1;
                    }
                    else {

                        POMCount[season][PlayerOfTheMatch] = 1;
                    }
                }
                else {

                    POMperPlayer[PlayerOfTheMatch] = 1;
                    POMCount[season] = POMperPlayer;
                }


            }
            for (const season in POMCount) {

                let playerWithPOM = Object.fromEntries(
                    Object.entries(POMCount[season]).sort((player1, player2) => { return player2[1] - player1[1] }).slice(0, 1)
                )
                PlayerWithHighestPOMPerSeason[season] = playerWithPOM;

            }

            let stringifiedObject = JSON.stringify(PlayerWithHighestPOMPerSeason, null, 2);

            fs.writeFile(outputFilePath, stringifiedObject, (err) => {

                if (err) {

                    console.log(err);
                }

                else {
                    
                    console.log("File has been created");
                }
            });

        });


    }
} catch (error) {
    console.log(error);
}

highestPOMeachSeason()
