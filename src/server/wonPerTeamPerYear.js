const fs = require('fs');
const csvToJson = require('../data/csvtoJSONConverter');

function wonPerTeamPerYear() {
    const matchesFilePath = 'src/data/matches.csv';
    const deliveriesFilePath = 'src/data/deliveries.csv';
    const outputFilePath = 'src/public/output.wonPerTeamPerYear.json';

    csvToJson(matchesFilePath).then((matches) => {
        try {
            if (Array.isArray(matches)) {

                let winningPerYear = {};

                for (let index = 0; index < matches.length; index++) {

                    let currentSeason = matches[index].season;
                    let currentWinningTeam = matches[index].winner;

                    if (currentSeason != null) {

                        if (winningPerYear.hasOwnProperty([currentSeason])) {


                            if (winningPerYear[currentSeason].hasOwnProperty(currentWinningTeam)) {
                                winningPerYear[currentSeason][currentWinningTeam] += 1;
                            }
                            else {
                                winningPerYear[currentSeason][currentWinningTeam] = 1;
                            }

                        }
                        else {
                            winningPerYear[currentSeason] = {};
                            winningPerYear[currentSeason][currentWinningTeam] = 1

                        }

                    }

                }
                let stringifiedObject = JSON.stringify(winningPerYear, null, 2);

                fs.writeFile(outputFilePath, stringifiedObject, (err) => {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log("File has been created");
                    }
                });

            }
            else {

                console.log("Unexpected Dataset Encoutered !");
            }

        }
        catch (error) {

            console.log(error);

        }

    });

}

wonPerTeamPerYear();