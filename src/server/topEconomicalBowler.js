const fs = require('fs');
const csvToJson = require('../data/csvtoJSONConverter');
const { matches } = require('lodash');

function top10EconomicalBowlers() {
    const matchesFilePath = 'src/data/matches.csv';
    const deliveriesFilePath = 'src/data/deliveries.csv';
    const outputFilePath = 'src/public/output.top10EconomicalBowler.json';

    csvToJson(deliveriesFilePath).then((deliveries) => {
        csvToJson(matchesFilePath).then((matches) => {
            try {
                if (Array.isArray(deliveries) && Array.isArray(matches)) {


                    let bowlerStats = {};

                    for (let index = 0; index < matches.length; index++) {

                        if (matches[index].season == "2015") {

                            let matchId = matches[index].id;

                            for (let index2 = 0; index2 < deliveries.length; index2++) {

                                if (deliveries[index2].match_id == matchId) {

                                    let currentObject = deliveries[index2];
                                    let currentBowler = deliveries[index2].bowler;

                                    if (bowlerStats[currentBowler]) {

                                        bowlerStats[currentBowler].runs += Number(currentObject.total_runs);

                                        if (currentObject.ball <= 6) {

                                            bowlerStats[currentBowler].balls += 1;
                                        }
                                    }
                                    else {

                                        bowlerStats[currentBowler] = {

                                            runs: parseInt(currentObject.total_runs),
                                            balls: 1

                                        }
                                    }
                                }
                            }
                        }
                    }

                    let bowlerEconomy = []

                    for (const bowler in bowlerStats) {

                        let obj = {};

                        obj.bowler = bowler;
                        obj.economy = (bowlerStats[bowler].runs / (bowlerStats[bowler].balls / 6))
                        bowlerEconomy.push(obj);
                    }

                    bowlerEconomy.sort((bowler1, bowler2) => { return bowler1.economy - bowler2.economy; });


                    let top10economicalBowlers = []

                    for (let index3 = 0; index3 < 10; index3++) {

                        top10economicalBowlers[index3] = bowlerEconomy[index3];
                    }

                    let stringifiedObject = JSON.stringify(top10economicalBowlers, null, 2);

                    fs.writeFile(outputFilePath, stringifiedObject, (err) => {

                        if (err) {

                            console.log(err);
                        }
                        else {
                            
                            console.log("File has been created");
                        }
                    });

                }
                else {

                    console.log("Unexpected Dataset Encoutered !");
                }

            }
            catch (error) {

                console.log(error);

            }

        });
    });
}
top10EconomicalBowlers()