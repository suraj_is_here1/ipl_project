const fs = require('fs')
const csvToJson = require('../data/csvtoJSONConverter');

try {
    function bestEconomyinSuperOver() {
        const matchesFilePath = 'src/data/matches.csv';
        const deliveriesFilePath = 'src/data/deliveries.csv';
        const outputFilePath = 'src/public/output.bestEconomyInSuperOver.json';

        let bestEconomicalBowler = {};

        csvToJson(deliveriesFilePath).then((deliveries) => {
            bowlerStat = {}
            for (const delivery of deliveries) {
                if (delivery.is_super_over == '1') {
                    let currentBowler = delivery.bowler;
                    let legbye = Number(delivery.legbye_runs);
                    let bye = Number(delivery.bye_runs);
                    let totalRun = Number(delivery.total_runs)
                    let runAndBall = {};

                    if (bowlerStat[currentBowler]) {
                        bowlerStat[currentBowler]["runs"] += (delivery.total_runs - (legbye + bye));
                        if (delivery.wide_runs == '0') {
                            bowlerStat[currentBowler]["balls"] += 1
                        }
                    }
                    else {
                        if (delivery.wide_runs) {
                            runAndBall["runs"] = (delivery.total_runs - (legbye + bye));
                            runAndBall["balls"] = 0;
                        }
                        else {
                            runAndBall["runs"] = (delivery.total_runs - (legbye + bye));
                            runAndBall["balls"] = 1;
                        }
                        bowlerStat[currentBowler] = runAndBall;
                    }
                }
            }
            let bowlersEconomy = {};
            for (const bowler in bowlerStat) {
                let economy = (bowlerStat[bowler].runs / bowlerStat[bowler].balls) * 6;
                bowlersEconomy[bowler] = economy;
            }

            bestEconomicalBowler = Object.fromEntries(Object.entries(bowlersEconomy).sort((bowler1, bowler2) => { return bowler1[1] - bowler2[1] }).slice(0, 1));

            let stringifiedObject = JSON.stringify(bestEconomicalBowler, null, 2)

            fs.writeFile(outputFilePath, stringifiedObject, (err) => {
                if (err) {
                    console.log(err);
                }
                else {
                    console.log("File Created Successfully");
                }
            })
        });
    }
}
catch (error) {
    console.log();
}
bestEconomyinSuperOver();

