const fs = require('fs');
const csvToJson = require('../data/csvtoJSONConverter');
const { matches } = require('lodash');

function extraRunConceeded() {
    const matchesFilePath = 'src/data/matches.csv';
    const deliveriesFilePath = 'src/data/deliveries.csv';
    const outputFilePath = 'src/public/output.extraRunPerTeam.json';

    csvToJson(deliveriesFilePath).then((deliveries) => {
        csvToJson(matchesFilePath).then((matches) => {
            try {
                if (Array.isArray(deliveries) && Array.isArray(matches)) {


                    let extraRunPerTeam = {};

                    for (let index = 0; index < matches.length; index++) {
                        if (matches[index].season == "2016") {

                            let matchId = matches[index].id;
                            for (let index2 = 0; index2 < deliveries.length; index2++) {

                                if (deliveries[index2].match_id == matchId) {

                                    let bowlingTeam = deliveries[index2].bowling_team;
                                    let extraRuns = deliveries[index2].extra_runs;

                                    if (extraRunPerTeam[bowlingTeam]) {
                                        
                                        extraRunPerTeam[bowlingTeam] += Number(extraRuns);
                                    }
                                    else {

                                        extraRunPerTeam[bowlingTeam] = Number(extraRuns);
                                    }

                                }
                            }
                        }
                    }



                    let stringifiedObject = JSON.stringify(extraRunPerTeam, null, 2);

                    fs.writeFile(outputFilePath, stringifiedObject, (err) => {
                        if (err) {
                            console.log(err);
                        }
                        else {
                            console.log("File has been created");
                        }
                    });

                }
                else {

                    console.log("Unexpected Dataset Encoutered !");
                }

            }
            catch (error) {

                console.log(error);

            }

        });
    });
}
extraRunConceeded()