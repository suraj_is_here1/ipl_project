const fs = require('fs');
const csvToJson = require('../data/csvtoJSONConverter');

function matchPerYear() {
    const matchesFilePath = 'src/data/matches.csv';
    const deliveriesFilePath = 'src/data/deliveries.csv';
    const outputFilePath = 'src/public/output.matchesPerYear.json';

    csvToJson(matchesFilePath).then((matches) => {
        try {
            if (Array.isArray(matches)) {

                let matchPerYear = {};

                for (let index = 0; index < matches.length; index++) {

                    let currentSeason = matches[index].season;

                    if (currentSeason != null) {

                        if (matchPerYear.hasOwnProperty([currentSeason])) {

                            matchPerYear[[currentSeason]] += 1;
                        }
                        else {
                            matchPerYear[[currentSeason]] = 1;
                        }

                    }

                }
                let stringifiedObject = JSON.stringify(matchPerYear, null, 2);

                fs.writeFile(outputFilePath, stringifiedObject, (err) => {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log("File has been created");
                    }
                });

            }
            else {

                console.log("Unexpected Dataset Encoutered !");
            }

        }
        catch (error) {

            console.log(error);

        }

    });

}

matchPerYear()






