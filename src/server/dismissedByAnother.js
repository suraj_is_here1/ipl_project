const fs = require('fs')
const csvToJson = require('../data/csvtoJSONConverter');
const { log } = require('console');

try {
    function dismissalByAnotherPlayer() {
        const matchesFilePath = 'src/data/matches.csv';
        const deliveriesFilePath = 'src/data/deliveries.csv';
        const outputFilePath = 'src/public/output.highestDismissalByAnotherPlayer.json';

        csvToJson(deliveriesFilePath).then((deliveries) => {
            let dismissal = {}
            for (const delivery of deliveries) {
                let batsman = delivery.batsman;
                let bowler = delivery.bowler;

                if (delivery.player_dismissed) {
                    if (dismissal[batsman]) {
                        if (dismissal[batsman][bowler]) {
                            dismissal[batsman][bowler] += 1
                        }
                        else {
                            dismissal[batsman][bowler] = 1
                        }
                    }
                    else {
                        dismissal[batsman] = { [bowler]: 1 }
                    }
                }
            }
            let finalDismissal = {};
            for (const batsman in dismissal) {
                let bowler = Object.fromEntries(
                    Object.entries(dismissal[batsman]).sort((bowler1, bowler2) => { return bowler2[1] - bowler1[1] }).slice(0, 1)
                )
                finalDismissal[batsman] = bowler
            }
            console.log(finalDismissal);

            let stringifiedObject = JSON.stringify(finalDismissal, null, 2)

            fs.writeFile(outputFilePath, stringifiedObject, (err) => {
                if (err) {
                    console.log(err);
                }
                else {

                    console.log("File created Successfully");
                }
            })

        })
    }
}
catch (error) {
    console.log(error);
}
dismissalByAnotherPlayer();
