const fs = require('fs');
const csvToJson = require('../data/csvtoJSONConverter');

try {
    function wonBothTossAndMatch() {
        const matchesFilePath = 'src/data/matches.csv';
        const deliveriesFilePath = 'src/data/deliveries.csv';
        const outputFilePath = 'src/public/output.wonBothTossAndMatch.json';


        csvToJson(matchesFilePath).then((match) => {
          
            let teamWonTossAndMatch = {};

            for (let index = 0; index < match.length; index++) {

                let teamWonMatch = match[index].winner;
                let teamWonToss = match[index].toss_winner;

                if (teamWonMatch == teamWonToss) {

                    if (teamWonTossAndMatch[teamWonMatch]) {

                        teamWonTossAndMatch[teamWonMatch] += 1;
                    }
                    else {

                        teamWonTossAndMatch[teamWonMatch] = 1;
                    }
                }

            }

            let stringifiedObject = JSON.stringify(teamWonTossAndMatch, null, 2);

            fs.writeFile(outputFilePath, stringifiedObject, (err) => {

                if (err) {
                    console.log(err);
                }

                else {
                    console.log("File has been created");
                }
            });

        });


    }
} catch (error) {
    console.log(error);
}
wonBothTossAndMatch();

