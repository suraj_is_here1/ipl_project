const csvtojson = require('csvtojson');

const csvToJson = (inputFilePath) => {
  return csvtojson().fromFile(inputFilePath).then((data) => {
    return data
  })
};

module.exports = csvToJson